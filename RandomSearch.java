import java.time.Instant;

public class RandomSearch {

    private int odlegloscOdOptimum(double calculatedOptimum, double opt) {
        double difference = calculatedOptimum - opt;
        double percentages = difference / opt;

        return (int) Math.round(percentages * 100);
    }

    public static void main(String[] args) {
                Data data = new Data("data/nug20.dat");
//        Data data = new Data("data/nug30.dat");
//        Data data = new Data("data/scr12.dat");
//        Data data = new Data("data/ste36c.dat");
//        Data data = new Data("data/sko90.dat");
//        Data data = new Data("data/sko100a.dat");
//        Data data = new Data("data/wil100.dat");
        double opt = ConfigMap.Nug20_OPT;
//        double opt = ConfigMap.Nug30_OPT;
//        double opt = ConfigMap.Scr12_OPT;
//        double opt = ConfigMap.Ste36c_OPT;
//        double opt = ConfigMap.Sko90_OPT;
//        double opt = ConfigMap.Sko100a_OPT;
//        double opt = ConfigMap.Wil100_OPT;
        WithRandomOpt randomOpt = new WithRandomOpt();
        RandomSearch randomSearch = new RandomSearch();

        double best;
        double profit;
        boolean progress;
        int worse;
        int steps;

        Instant start = Instant.now();
        for (int j = 0; j < 10; j++) {
            worse = 0;
            steps = 0;
            progress = true;

            best = randomOpt.calculateCost(randomOpt.randomPermutation(data.getSize()), data);

            while (progress) {
                steps++;  //zliczanie wszystkich przejść
                profit = randomOpt.calculateCost(randomOpt.randomPermutation(data.getSize()), data);

                if (profit < best) {
                    worse = 0; //zerowanie jeśli znalazł lepszy
                    best = profit;
                } else {
                    worse++; // zwiększenie licznika błędu jeśli wynik się nie poprawił
                }

                if (worse >= 99999) {  // koniec jeśli wartość nieudanych prób osiągnie zadaną wartość
                    progress = false;

                    System.out.println("WARTOSC: " + best
                            + "     kroki: " + steps
                            + "     ODLEGLOSC: " + randomSearch.odlegloscOdOptimum(best, opt)
                    );
                }
            }
        }

        Instant end = Instant.now();
        long time = end.toEpochMilli() - start.toEpochMilli();
        System.out.println("CZAS: " + time);

    }
}
