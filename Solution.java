public class Solution {

    public static double calculateCost(int[] solution, Data data) {
        int cost = 0;

        for (int i = 0; i < data.getSize(); i++) {  //wiersz
            for (int j = 0; j < data.getSize(); j++) {  //kolumna
                cost = cost + data.getMatrixA(i, j) * data.getMatrixB(solution[i] - 1, solution[j] - 1);
            }
        }

        return cost;
    }

    public static void main(String[] args) {
        Data data = new Data("data/nug20.dat");
        int[] opt = {18, 14, 10, 3, 9, 4, 2, 12, 11, 16, 19, 15, 20, 8, 13, 17, 5, 7, 1, 6};

        for (int i =1; i < 20; i++)
        System.out.println(calculateCost(opt, data));

    }
}
