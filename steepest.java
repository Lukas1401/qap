import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class steepest {
    private static int size = 0;

    public static int[] randomPermutation() {

        int[] permutation = new int[size];
        for (int i = 0; i < size; i++) {
            permutation[i] = i;
        }

        List<Integer> list = new ArrayList<>();
        for (int i : permutation) {
            list.add(i);
        }

        Collections.shuffle(list);

        for (int i = 0; i < list.size(); i++) {
            permutation[i] = list.get(i);
        }

        return permutation;
    }

    public static double calculateCost(int[] solution, Data data) {
        int cost = 0;
        int size = data.getSize();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                cost = cost + data.getMatrixA(i, j) * data.getMatrixB(solution[i], solution[j]);
            }
        }

        return cost;
    }

    private static List<int[]> getNeighbours(int[] actualSolution) {
        int[] tour = actualSolution;
        List<int[]> listNeigh = new ArrayList<>();

        for (int i = 0; i < tour.length; i++) {
            int a = tour[i];
            for (int j = 0; j < tour.length; j++) {
                int b = tour[j];
                int[] neighbour = tour.clone();

                neighbour[i] = b;
                neighbour[j] = a;

                if (j > i) {
                    listNeigh.add(neighbour);
                }
            }
        }

        return listNeigh;
    }

    private static int distanceFromOptimum(double calculatedOptimum, double opt) {
        double difference = calculatedOptimum - opt;
        double percentages = difference / opt;

        return (int) Math.round(percentages * 100);
    }


    public static void main(String[] args) {
//        Data data = new Data("data/nug20.dat");
//        Data data = new Data("data/nug30.dat");
//        Data data = new Data("data/scr12.dat");
//        Data data = new Data("data/ste36c.dat");
//        Data data = new Data("data/sko90.dat");
//        Data data = new Data("data/sko100a.dat");
        Data data = new Data("data/wil100.dat");
//        double opt = ConfigMap.Nug20_OPT;
//        double opt = ConfigMap.Nug30_OPT;
//        double opt = ConfigMap.Scr12_OPT;
//        double opt = ConfigMap.Ste36c_OPT;
//        double opt = ConfigMap.Sko90_OPT;
//        double opt = ConfigMap.Sko100a_OPT;
        double opt = ConfigMap.Wil100_OPT;
        size = data.getSize();
        boolean progress = true;
        data.displayMatrixA();
        List<int[]> list;
        double theLowestCost;
        double neighbourCost;
        double bestNeighbourCost = 0;


        Instant start = Instant.now();
        for (int j = 0; j < 10; j++) {
            int stepsNumber = 0;   //przejscie do sasiada
            int ratedSolution = 0;   // sprawdzanie sasiadow
            int[] actualSolution = randomPermutation();
            int[] bestNeighbour = new int[0];
            theLowestCost = calculateCost(actualSolution, data);

            progress = true;
            while (progress) {
                stepsNumber++;
                list = getNeighbours(actualSolution);
                progress = false;
                bestNeighbourCost = theLowestCost;
                for (int i = 0; i < list.size(); i++) {
                    ratedSolution++;
                    int[] neighbour = list.get(i);
                    neighbourCost = calculateCost(neighbour, data);
                    if (neighbourCost < bestNeighbourCost) {
                        bestNeighbourCost = neighbourCost;
                        bestNeighbour = neighbour;
                    }
                }

                if (bestNeighbourCost < theLowestCost) {
                    actualSolution = bestNeighbour;
                    theLowestCost = bestNeighbourCost;
                    progress = true;
                }
            }
            System.out.println("WARTOSC: " + theLowestCost
                    + "     KROKI: " + stepsNumber
                    + "     OCENIANE ROZWIAZANIA: " + ratedSolution
                    + "             ODLEGLOSC: " + distanceFromOptimum(theLowestCost, opt)
            );
        }
        Instant end = Instant.now();
        long time = end.toEpochMilli() - start.toEpochMilli();
        System.out.println("CZAS: " + time);
    }
}