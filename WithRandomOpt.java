public class WithRandomOpt {

    public int[] randomPermutation(int size) {
        int[] a = new int[size];

        // insert integers 0..n-1
        for (int i = 0; i < size; i++) {
            a[i] = i;
        }

        // shuffle
        for (int i = 0; i < size; i++) {
            int r = (int) (Math.random() * (i + 1));     // int between 0 and i
            int swap = a[r];
            a[r] = a[i];
            a[i] = swap;
        }

        return a;
    }

    public double calculateCost(int[] solution, Data data) {
        int cost = 0;

        for (int i = 0; i < data.getSize(); i++) {  //wiersz
            for (int j = 0; j < data.getSize(); j++) {  //kolumna
                cost = cost + data.getMatrixA(i, j) * data.getMatrixB(solution[i], solution[j]);
            }
        }

        return cost;
    }

    public static void main(String[] args) {
        Data data = new Data("data/nug20.dat");
        WithRandomOpt randomOpt = new WithRandomOpt();

        for (int i = 1; i < 10; i++)
            System.out.println(randomOpt.calculateCost(randomOpt.randomPermutation(20), data));
    }
}