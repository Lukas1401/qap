public final class ConfigMap {

    public static final double Nug20_OPT = 2570;
    public static final double Nug30_OPT = 6124;
    public static final double Scr12_OPT = 31410;
    public static final double Ste36c_OPT = 8239110;
    public static final double Sko90_OPT = 115534;
    public static final double Sko100a_OPT = 152002;
    public static final double Wil100_OPT = 273038;

}
